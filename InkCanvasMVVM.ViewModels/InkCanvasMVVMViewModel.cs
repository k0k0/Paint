﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using InkCanvasMVVM.ViewModels.Commands;
using InkCanvasMVVM.ViewModels.Helpers;

namespace InkCanvasMVVM.ViewModels
{
    public class InkCanvasMVVMViewModel
        : ObservedObject
    {

        public bool canCloseAp { get; set; }
        public bool isCloseFromMenu { get; set; }

        public DrawCommand DrawCommand { get; set; }
        public SelectCommand SelectCommand { get; set; }
        public AboutMeCommand AboutMeCommand { get; set; }
        public ChangeColorCommand ChangeColorCommand { get; set; }

        private InkCanvasEditingMode editingMode;
        private Brushes changeColor;
        private InkCanvas greenColor;
        private RelayCommand closeWindowCommand;

        public ICommand CloseWindowCommand
        {
            get => closeWindowCommand;
        }


        public InkCanvasEditingMode EditingMode
        {
            get => editingMode;
            set
            {
                editingMode = value;
                OnPropertyChanged("EditingMode");
            }
        }

        public Brushes ChangeColor
        {
            get => changeColor;
            set
            {
                if (changeColor != value)
                {
                    changeColor = value;
                    OnPropertyChanged(nameof(ChangeColorCommand));
                }
            }
        }

        public InkCanvas GreenColor
        {
            get => greenColor;
            set
            {
                if (greenColor != value)
                {
                    greenColor = value;
                    OnPropertyChanged(nameof(GreenColorCommand));
                }
                
            }
        }

        public InkCanvasMVVMViewModel()
        {
            DrawCommand = new DrawCommand(this);
            SelectCommand = new SelectCommand(this);
            closeWindowCommand = new RelayCommand(CloseWindow_Command);
            AboutMeCommand = new AboutMeCommand(this);
            ChangeColorCommand = new ChangeColorCommand(this);

            isCloseFromMenu = false;
            canCloseAp = true;
        }

        private void CloseWindow_Command(object obj)
        {
            isCloseFromMenu = true;
            AskClosingQuestion(obj);
        }

        private void AskClosingQuestion(object obj)
        {
            var result = MessageBox.Show("Close program?", "Are you sure?",
                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);

            canCloseAp = result != MessageBoxResult.No;

            if (canCloseAp)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
