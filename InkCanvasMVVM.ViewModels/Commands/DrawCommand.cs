﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace InkCanvasMVVM.ViewModels.Commands
{
    public class DrawCommand
        : ICommand
    {
        private readonly InkCanvasMVVMViewModel vm;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            vm.EditingMode = InkCanvasEditingMode.Ink;
        }

        public event EventHandler CanExecuteChanged;

        public DrawCommand(InkCanvasMVVMViewModel vm)
        {
            this.vm = vm;
        }
    }
}
