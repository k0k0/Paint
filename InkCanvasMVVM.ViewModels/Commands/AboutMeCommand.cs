﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace InkCanvasMVVM.ViewModels.Commands
{
    public class AboutMeCommand
        : ICommand
    {
        private readonly InkCanvasMVVMViewModel vm;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            AboutMe(parameter);
        }

        public event EventHandler CanExecuteChanged;

        public AboutMeCommand(InkCanvasMVVMViewModel vm)
        {
            this.vm = vm;
        }

        private void AboutMe (object parameter)
        {
            var result = MessageBox.Show("Program wykonany przez Andrzeja Sobczyka", "Little Paint",
                MessageBoxButton.OK, MessageBoxImage.Information);

        }
    }
}
