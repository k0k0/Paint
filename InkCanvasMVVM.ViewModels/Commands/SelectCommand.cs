﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;

namespace InkCanvasMVVM.ViewModels.Commands
{
    public class SelectCommand
        : ICommand
    {
        private readonly InkCanvasMVVMViewModel vm;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            vm.EditingMode = InkCanvasEditingMode.Select;
        }

        public event EventHandler CanExecuteChanged;

        public SelectCommand(InkCanvasMVVMViewModel vm)
        {
            this.vm = vm;
        }
    }
}
